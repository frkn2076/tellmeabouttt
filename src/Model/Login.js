import Sequelize from 'sequelize'

import DatabaseConfig from 'Config/DatabaseConfig';

const Model = Sequelize.Model;
const sequelize = DatabaseConfig


class Login extends Model { }


Login.init({
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    mailKey: Sequelize.STRING,
    isActive: Sequelize.BOOLEAN
}, { sequelize, modelName: 'Login' });

Login.sync({
    // force:true
})


export default Login;
